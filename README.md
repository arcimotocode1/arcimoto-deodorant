# Arcimoto Deodorant

In order to diversify our revenue streams, Arcimoto plans to start selling deodorant online!

The marketing department has decided on the enclosed design *DeodorantMockWebsite.psd* for the sales portal, and it's your job to turn the dream into a responsive, accessible and standards compliant web page.

Inside the *assets* directory are the images provided by the marketing team. It is your job to use these assets to best accomplish the desired look and feel.

In addition to the static layout, the circular area with the Next/Previous buttons should cycle through images loaded from a remote API of your choice (eg, Imgur, IMDB, BoardGameGeek, etc).